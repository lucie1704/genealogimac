#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: promotion
#------------------------------------------------------------

CREATE TABLE promotion(
        finish_year Year NOT NULL
	,CONSTRAINT promotion_PK PRIMARY KEY (finish_year)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: student
#------------------------------------------------------------

CREATE TABLE student(
        idStudent                Int  Auto_increment  NOT NULL ,
        first_name               Varchar (50) NOT NULL ,
        last_name                Varchar (50) NOT NULL ,
        birthdate                Date NOT NULL ,
        finish_year              Year NOT NULL ,
        id_mentor   			 Int ,
        id_godchild_of 			 Int ,
		CONSTRAINT student_PK PRIMARY KEY (idStudent)
)ENGINE=InnoDB;




ALTER TABLE student
	ADD CONSTRAINT student_promotion0_FK
	FOREIGN KEY (finish_year)
	REFERENCES promotion(finish_year);

ALTER TABLE student
	ADD CONSTRAINT student_student1_FK
	FOREIGN KEY (idStudent_is_mentor_of)
	REFERENCES student(idStudent);

ALTER TABLE student
	ADD CONSTRAINT student_student2_FK
	FOREIGN KEY (idStudent_is_godchild_of)
	REFERENCES student(idStudent);
